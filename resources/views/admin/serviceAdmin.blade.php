@extends('admin.includes.app')
@section('content')
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<!-- <h2><strong>Services</strong></h2> -->
  <div class="alert alert-success" style="display:none">
      <strong>Success!</strong> Successfully Compleated !
    </div>
 
    <div class="alert alert-danger" style="display:none">
    </div>
    <div style="width: 15%; margin-bottom: 10px">
        <button type="button" id="newcategory" class="btn btn-block btn-success" data-toggle="modal" data-target="#exampleModal">New Category</button>
    </div>
    <br>
    
     <table id="example" class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
        <thead>
            <tr>

                <th>ID</th>
                <th>Icon</th>
                <th>Name</th>
                <th>Parent_id</th>
                <th>Depth</th>
                <th>Picture</th>
                <th>Description</th>
               <th>Option</th>


                
            </tr>
        </thead>

        <tfoot>
            <tr>
               <th>ID</th>
                <th>Icon</th>
                <th>Name</th>
                <th>Parent_id</th>
                <th>Depth</th>
                <th>Picture</th>
                <th>Description</th>
                <th>Option</th>
                <!-- <th>Added On</th> -->
                
            </tr>
        </tfoot>
        <tbody>
          @foreach($service as $s)
           <tr>
          <td>{{$s->id}}</td>
          <td>{{$s->icon}}</td>
          <td>{{$s->name}}</td>
          <td>{{$s->parent_id}}</td>
          <td>{{$s->depth}}</td>
          <td>{{$s->picture}}</td>
          <td>{{$s->description}}</td>

          <td>
      <a class="edit_button" href="#" data-toggle="modal" data-target="#exampleModal">Edit</a> | 
                    
      <a href="{{route('service_delete',$s->id)}}" >Delete</a>
        </td>
     
          
         </tr>
         @endforeach 
         </tbody>

    </table>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Service </h4>
          </div>
          <div class="modal-body">
            <form id="editcategory">
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id">
              <div class="form-group">
                <label for="name" class="control-label">Icon:</label>
                <input type="text" class="form-control" id="icon" name="icon">
              </div>
               <div class="form-group">
                <label  for="image" class="control-label">Name Product:</label>
                <input type="text" class="form-control" id="name" name="name">
              </div>
              <div class="form-group">
                <label for="sel1">Parent_id:</label>
           
                <select class="form-control" id="sel1" name="parent_id">
                    @foreach($service as $s)
                     <option>0</option>
                  <option>{{$s->id}}</option>
                 @endforeach
                </select>
                
              </div>
              
               <div class="form-group">
                <label for="description" class="control-label">Picture Service:</label>
                <textarea class="form-control" id="picture" name="picture"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Description:</label>
                <textarea class="form-control" id="description" name="description"></textarea>
              </div>
              
              <!-- <div class="form-group">
                <label for="status" class="control-label">Status:</label>
                <select class="form-control" id="status" name="status">
                  <option value="active">Active</option>
                  <option value="inactive">Inactive</option>
                </select>
              </div> -->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save" data-dismiss="modal">Save Category</button>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('after_scripts')

 <script type="text/javascript">
$(document).ready(function() {
    var table = $('.datatable').DataTable();

    $('#newcategory').click(function() {
        $('#id').val('');
        $('#icone').val('');
        $('#name').val('');
        $('#parent_id').val('');
        $('#depth').val('');
        $('#picture').val('');
        $('#description').val('');




        $('#exampleModalLabel').val('New Category');
    });
    
    $('.datatable tbody').on( 'click', 'a.edit_button', function () {
        var category = table.row( $(this).parents('tr') ).data();
        console.log( category );

        //$('.dropdown-toggle').dropdown()
        $('#id').val(category[0]);
        $('#icon').val(category[1]);
        $('#name').val(category[2]);
        $('#parent_id').val(category[3]);
        $('#depth').val(category[4]);
        $('#picture').val(category[5]);
        $('#description').val(category[6]);




        
    });

    $('#save').click(function(){
      // Use Ajax to submit form data
        
      var editOrNew = $('#exampleModalLabel').val();
      var url = '/update_service';
      if(editOrNew === 'New Category') {
          url = '/save_service';
      }
      $.ajax({
          url: url,
          type: 'POST',
          data: $('#editcategory').serialize(),
          success: function(result) {
              // ... Process the result ...
              if(result.status == 'success')
              {
                $('.alert-success').show();
                window.setTimeout(function () {
                    $('.alert-success').hide();
                }, 1000);

              } else {
                $('.alert-danger').show();
                window.setTimeout(function () {
                    $('.alert-danger').hide();
                }, 1000);
              }
              console.log(result);
          }
      });
        
    })

});
</script>
@endpush()