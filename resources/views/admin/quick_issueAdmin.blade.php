@extends('admin.includes.app')
@section('content')
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<!-- <h2><strong>Services</strong></h2> -->
  <div class="alert alert-success" style="display:none">
      <strong>Success!</strong> Successfully Compleated !
    </div>
 
    <div class="alert alert-danger" style="display:none">
    </div>
    <div style="width: 15%; margin-bottom: 10px">
        <button type="button" id="newcategory" class="btn btn-block btn-success" data-toggle="modal" data-target="#exampleModal">New Category</button>
    </div>
    <br>
    
     <table id="example" class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
        <thead>
            <tr>

                <th>ID</th>
                <th>Icon</th>
                <th>Name</th>
                <th>Option</th>


                
            </tr>
        </thead>

        <tfoot>
            <tr>
                 <th>ID</th>
                <th>Icon</th>
                <th>Name</th>
                <th>Option</th>
                <!-- <th>Added On</th> -->
                
            </tr>
        </tfoot>
        <tbody>
          @foreach($quick_issue as $s)
           <tr>
            <td>{{$s->id}}</td>
            <td>{{$s->icon}}</td>
            <td>{{$s->name}}</td>

            <td>
      <a class="edit_button" href="#" data-toggle="modal" data-target="#exampleModal">Edit</a> | 
                    
      <a href="{{route('quick_service_issue_delete',$s->id)}}" >Delete</a>
        </td>
          





     
          
         </tr>
         @endforeach 
         </tbody>

    </table>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Quick Issue</h4>
          </div>
          <div class="modal-body">
            <form id="editcategory">
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id">
              <div class="form-group">
                <label for="name" class="control-label">Icon Product:</label>
                <input type="text" class="form-control" id="icon" name="icon">
              </div>
               <div class="form-group">
                <label  for="image" class="control-label">Product Name:</label>
                <input type="text" class="form-control" id="name" name="name">
              </div>
              
              
              <!-- <div class="form-group">
                <label for="status" class="control-label">Status:</label>
                <select class="form-control" id="status" name="status">
                  <option value="active">Active</option>
                  <option value="inactive">Inactive</option>
                </select>
              </div> -->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save" data-dismiss="modal">Save Category</button>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('after_scripts')

 <script type="text/javascript">
$(document).ready(function() {
    var table = $('.datatable').DataTable();

    $('#newcategory').click(function() {
        //$('#id').val('');
        $('#icon').val('');
        $('#name').val('');
        
        $('#exampleModalLabel').val('New Category');
    });
    
    $('.datatable tbody').on( 'click', 'a.edit_button', function () {
        var category = table.row( $(this).parents('tr') ).data();
        console.log( category );

        //$('.dropdown-toggle').dropdown()
        $('#id').val(category[0]);
        $('#icon').val(category[1]);
        $('#name').val(category[2]);
    

        
    });

    $('#save').click(function(){
      // Use Ajax to submit form data
        
      var editOrNew = $('#exampleModalLabel').val();
      var url = '/update_quick_issue';
      if(editOrNew === 'New Category') {
          url = '/save_quick_issue';
      }
      $.ajax({
          url: url,
          type: 'POST',
          data: $('#editcategory').serialize(),
          success: function(result) {
              // ... Process the result ...
              if(result.status == 'success')
              {
                $('.alert-success').show();
                window.setTimeout(function () {
                    $('.alert-success').hide();
                }, 1000);

              } else {
                $('.alert-danger').show();
                window.setTimeout(function () {
                    $('.alert-danger').hide();
                }, 1000);
              }
              console.log(result);
          }
      });
        
    })

});
</script>
@endpush()