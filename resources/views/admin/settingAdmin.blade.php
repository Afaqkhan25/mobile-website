@extends('admin.includes.app')
@section('content')
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<!-- <h2><strong>Services</strong></h2> -->
  <div class="alert alert-success" style="display:none">
      <strong>Success!</strong> Successfully Compleated !
    </div>
 
    <div class="alert alert-danger" style="display:none">
    </div>
    <div style="width: 15%; margin-bottom: 10px">
        <button type="button" id="newcategory" class="btn btn-block btn-success" data-toggle="modal" data-target="#exampleModal">New Category</button>
    </div>
    <br>
    
    <table id="example" class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
        <thead>
            <tr>

                <th>ID</th>
                <th>Logo</th>
                <th>about</th>
                <th>Title</th>
                <th>Title_description</th>
                <th>Picture_about</th>
                <th>slider_image</th>

                <th>Options</th>
                


                
            </tr>
        </thead>

        <tfoot>
            <tr>
               <th>ID</th>
                <th>Logo</th>
                <th>about</th>
                <th>Title</th>
                <th>Title_description</th>
                <th>Picture_about</th>
                <th>slider_image</th>

                <th>Options</th>
                
                <!-- <th>Added On</th> -->
                
            </tr>
        </tfoot>
        <tbody>
          @foreach($setting as $s)
           <tr>
            <td>{{$s->id}}</td>
            <td>{{$s->logo}}</td>
            <td>{{$s->about}}</td>
            <td>{{$s->title}}</td>
            <td>{{$s->title_description}}</td>
            <td>{{$s->picture_about}}</td>
            <td>{{$s->slider_image}}</td>

                <td><a class="edit_button" href="#" data-toggle="modal" data-target="#exampleModal">Edit</a> | 
                    
        <a href="{{route('setting_delete',$s->id)}}" >Delete</a>
        </td>




     
          
         </tr>
         @endforeach 
         </tbody>

    </table>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Settings</h4>
          </div>
          <div class="modal-body">
            <form id="editcategory">
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id">
            <div class="form-group">
                <label  for="image" class="control-label">LOGO:</label>
                <input type="text" class="form-control" id="logo" name="logo">
              </div>
              <div class="form-group">
                <label for="description" class="control-label">About:</label>
                <textarea class="form-control" id="about" name="about"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Title:</label>
                <textarea class="form-control" id="title" name="title"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Title_Description:</label>
                <textarea class="form-control" id="title_description" name="title_description"></textarea>
              </div>
             <div class="form-group">
                <label  for="image" class="control-label">Picture_about:</label>
                <input type="text" class="form-control" id="picture_about" name="picture_about">
              </div>
              <div class="form-group">
                <label  for="image" class="control-label">slider_image:</label>
                <input type="text" class="form-control" id="slider_image" name="slider_image">
              </div>
              
              <!-- <div class="form-group">
                <label for="status" class="control-label">Status:</label>
                <select class="form-control" id="status" name="status">
                  <option value="active">Active</option>
                  <option value="inactive">Inactive</option>
                </select>
              </div> -->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save" data-dismiss="modal">Save Setting</button>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('after_scripts')

 <script type="text/javascript">
$(document).ready(function() {
    var table = $('.datatable').DataTable();

    $('#newcategory').click(function() {
        $('#id').val('');
        $('#logo').val('');
        $('#title').val('');
        $('#title_description').val('');
        $('#picture_about').val('');
        $('#slider_image').val('');


        $('#exampleModalLabel').val('New Category');
    });
    
    $('.datatable tbody').on( 'click', 'a.edit_button', function () {
        var category = table.row( $(this).parents('tr') ).data();
        console.log( category );

        //$('.dropdown-toggle').dropdown()
        $('#id').val(category[0]);
        $('#logo').val(category[1]);
        $('#about').val(category[2]);

        $('#title').val(category[3]);
        $('#title_description').val(category[4]);
        $('#picture_about').val(category[5]);
        $('#slider_image').val(category[6]);


        
    });

    $('#save').click(function(){
      // Use Ajax to submit form data
        
      var editOrNew = $('#exampleModalLabel').val();
      var url = '/update_settings';
      if(editOrNew === 'New Category') {
          url = '/save_settings';
      }
      $.ajax({
          url: url,
          type: 'POST',
          data: $('#editcategory').serialize(),
          success: function(result) {
              // ... Process the result ...
              if(result.status == 'success')
              {
                $('.alert-success').show();
                window.setTimeout(function () {
                    $('.alert-success').hide();
                }, 1000);

              } else {
                $('.alert-danger').show();
                window.setTimeout(function () {
                    $('.alert-danger').hide();
                }, 1000);
              }
              console.log(result);
          }
      });
        
    })

});
</script>
@endpush()