<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <!-- <form action="" method="get" class="sidebar-form">
          
        <div class="input-group">
          <input type="text" name="q" id="q" class="form-control" placeholder="Search Quiz|Categoroy">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>    -->
        
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">Content Management</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{route('services_admin')}}"><i class="fa fa-link"></i><span>Services</span></a></li>
        <li class="active"><a href="{{route('setting_admin')}}"><i class="fa fa-link"></i><span>Setting</span></a></li>
        <li class="active"><a href="{{route('claim_admin')}}"><i class="fa fa-link"></i><span>Claim</span></a></li>
        <li class="active"><a href="{{route('contect_admin')}}"><i class="fa fa-link"></i><span>Contect Us</span></a></li>
        <li class="active"><a href="{{route('quick_claim')}}"><i class="fa fa-link"></i><span>Quick_Claim</span></a></li>
        <li class="active"><a href="{{route('quick_Service')}}"><i class="fa fa-link"></i><span>Quick_Service</span></a></li>
        <li class="active"><a href="{{route('quick_issue')}}"><i class="fa fa-link"></i><span>Quick_issue</span></a></li>




         <!-- <li class="active"><a href=""><i class="fa fa-link"></i><span>Brands</span></a></li>
        <li class="active"><a href=""><i class="fa fa-link"></i> <span>Products</span></a></li> -->
          <!-- <li class="active"><a href=""><i class="fa fa-link"></i> <span>Featured Quizzes</span></a></li>
		   <li class="active"><a href=""><i class="fa fa-link"></i> <span>Manage Users</span></a></li> -->
        <!-- <li><a href="#"><i class="fa fa-link"></i> <span>Questions</span></a></li> -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>