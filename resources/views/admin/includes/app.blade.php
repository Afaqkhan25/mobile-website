<!DOCTYPE html>
<html>
	<head>
		@stack('before_styles')
		@include('admin.includes.styles')
		@stack('after_styles')
	</head>
	<title>Celfixx Admin</title>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			@include('admin.includes.adminheader')
			@include('admin.includes.sidebar')
			<div class="content-wrapper">
			    <!-- Content Header (Page header) -->
			    <section class="content-header">
			      <h1 style="color:darkblue">
			       Celfixx		        
			      </h1>
			      
			      <ol class="breadcrumb">
			        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
			        <li class="active">Here</li>
			      </ol>
			    </section>

			    <table>
			    	<tr>
			    		
			    	</tr>
			    </table>

			    <!-- Main content -->
			    <section class="content">
					@yield('content')
				</section>
    			<!-- /.content -->
  			</div>
			@include('admin.includes.adminfooter')
		</div>
		@stack('before_scripts')
		@include('admin.includes.requiredjs')
		@stack('after_scripts')
	</body>
</html>