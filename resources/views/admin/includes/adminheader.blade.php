<!-- Main Header -->
<header class="main-header">

  <!-- Logo -->

  <a href="/admin" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>C</b>F</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Celfixx-</b>Services</span>
  </a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
       <li class="dropdown notifications-menu">
        <!-- Menu toggle button -->
        <a href="" >
          <i class="fa fa-user"> Main Site</i>
        </a>
      </li>
      <li class="dropdown notifications-menu">
        <!-- Menu toggle button -->
       <a href="{{route('parentServices')}}">Logout</a>
    </li>
    <!-- Messages: style can be found in dropdown.less-->
    
    <!-- Control Sidebar Toggle Button -->
    <li>
      <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
    </li>
  </ul>
  <form id="logout-form" action="" method="POST" style="display: none;">
    {{ csrf_field() }}
  </form>
</div>
</nav>
</header>