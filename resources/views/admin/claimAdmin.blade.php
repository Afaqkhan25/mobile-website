@extends('admin.includes.app')
@section('content')
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<!-- <h2><strong>Services</strong></h2> -->
  <div class="alert alert-success" style="display:none">
      <strong>Success!</strong> Successfully Compleated !
    </div>
 
    <div class="alert alert-danger" style="display:none">
    </div>
   <!--  <div style="width: 15%; margin-bottom: 10px">
        <button type="button" id="newcategory" class="btn btn-block btn-success" data-toggle="modal" data-target="#exampleModal">New Category</button>
    </div>
 -->    <br>
    
     <table id="example" class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
        <thead>
            <tr>

                <th>ID</th>
                <th>Service</th>
                <th>Model</th>
                <th>Issue</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Company</th>
                <th>Location</th>
                <th>Promo Code</th>
                <th>Message</th>
                <th>Option</th>


                
            </tr>
        </thead>

        <tfoot>
            <tr>
               <th>ID</th>
                <th>Service</th>
                <th>Model</th>
                <th>Issue</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Company</th>
                <th>Location</th>
                <th>Promo Code</th>
                <th>Message</th>
              <th>Option</th>
                <!-- <th>Added On</th> -->
                
            </tr>
        </tfoot>
        <tbody>
        @foreach($claim as $c)
        <tr>
           
              
          <td>{{$c->id}}</td>
          <td>{{$c->service}}</td>
          <td>{{$c->model}}</td>
          <td>{{$c->issue}}</td>
          <td>{{$c->name}}</td>
          <td>{{$c->email}}</td>
          <td>{{$c->mobile}}</td>
          <td>{{$c->company}}</td>
          <td>{{$c->location}}</td>
          <td>{{$c->promo_code}}</td>
          <td>{{$c->message}}</td>
      <td>
      <a class="edit_button" href="#" data-toggle="modal" data-target="#exampleModal">Edit</a> | 
                    
      <a href="{{route('claim_delete',$c->id)}}" >Delete</a>
        </td>
           @endforeach
         </tbody>

    </table>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Claim Details</h4>
          </div>
          <div class="modal-body">
            <form id="editcategory">
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id">
              <div class="form-group">
                <label for="name" class="control-label">Service:</label>
                <input type="text" class="form-control" id="service" name="service">
              </div>
               <div class="form-group">
                <label  for="image" class="control-label">Model:</label>
                <input type="text" class="form-control" id="model" name="model">
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Issue:</label>
                <textarea class="form-control" id="issue" name="issue"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Name:</label>
                <textarea class="form-control" id="name" name="name"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Email :</label>
                <textarea class="form-control" id="email" name="email"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Mobile:</label>
                <textarea class="form-control" id="mobile" name="mobile"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Company:</label>
                <textarea class="form-control" id="company" name="company"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Location:</label>
                <textarea class="form-control" id="location" name="location"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Promo Code:</label>
                <textarea class="form-control" id="promo_code" name="promo_code"></textarea>
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Message:</label>
                <textarea class="form-control" id="message" name="message"></textarea>
              </div>
              <!-- <div class="form-group">
                <label for="status" class="control-label">Status:</label>
                <select class="form-control" id="status" name="status">
                  <option value="active">Active</option>
                  <option value="inactive">Inactive</option>
                </select>
              </div> -->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save" data-dismiss="modal">Save Category</button>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('after_scripts')

 <script type="text/javascript">
$(document).ready(function() {
    var table = $('.datatable').DataTable();

    $('#newcategory').click(function() {
        $('#id').val('');
        $('#service').val('');
        $('#model').val('');
        $('#issue').val('');
        $('#name').val('');
        $('#email').val('');
        $('#mobile').val('');
        $('#company').val('');
        $('#location').val('');
        $('#promo_code').val('');
        $('#message').val('');
$('#exampleModalLabel').val('New Category');
    });
    
    $('.datatable tbody').on( 'click', 'a.edit_button', function () {
        var category = table.row( $(this).parents('tr') ).data();
        console.log( category );

        //$('.dropdown-toggle').dropdown()
        $('#id').val(category[0]);
        $('#service').val(category[1]);
        $('#model').val(category[2]);
        $('#issue').val(category[3]);
        $('#name').val(category[4]);
        $('#email').val(category[5]);
        $('#mobile').val(category[6]);
        $('#company').val(category[7]);
        $('#location').val(category[8]);
        $('#promo_code').val(category[9]);
        $('#message').val(category[10]);








        
    });

    $('#save').click(function(){
      // Use Ajax to submit form data
        
      var editOrNew = $('#exampleModalLabel').val();
      var url = '/update_claim';
      if(editOrNew === 'New Category') {
          url = '/create/category';
      }
      $.ajax({
          url: url,
          type: 'POST',
          data: $('#editcategory').serialize(),
          success: function(result) {
              // ... Process the result ...
              if(result.status == 'success')
              {
                $('.alert-success').show();
                window.setTimeout(function () {
                    $('.alert-success').hide();
                }, 1000);

              } else {
                $('.alert-danger').show();
                window.setTimeout(function () {
                    $('.alert-danger').hide();
                }, 1000);
              }
              console.log(result);
          }
      });
        
    })

});
</script>
@endpush()