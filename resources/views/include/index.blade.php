<!DOCTYPE html>
<html lang="en" class="no-js">
   <!-- Begin Head -->
   <head>
      <!-- Basic Meta Tag -->
      <meta charset="utf-8" />
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Celfixx" />
      <meta name="description" content="celfixx provide the services for repairing mobile and tablet">
      <meta name="author" content="Celcare">
      <meta http-equiv="Cache-control" content="public">
      <title>Celfixx</title>
      <!-- Web Fonts -->
      <link href="{{asset('https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700')}}" rel="stylesheet">
      <!-- Vendor Styles -->
      <link rel="icon" href="{{asset('img/fav.png')}}" type="image/gif" sizes="16x16">
      
      <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Favicon -->
      <!-- <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon"> -->
   </head>
   <!-- End Head -->
   <!-- Body -->
   <body>
      <!--========== HEADER ==========-->
      <header class="navbar-fixed-top s-header js__header-sticky js__header-overlay">
         <!-- Navbar -->
         <div class="s-header__navbar">
            <div class="s-header__container">
               <div class="s-header__navbar-row">
                  <div class="s-header__navbar-row-col">
                     <div class="s-header__logo ">
                        @foreach($setting as $s)
                        <a class="navbar-brand s-header__logo-link s-header__logo-img-default" href="{{route('parentServices')}}">
                           <h3 style="color: #f9f9f9;">{{$s->logo}}</h3>
                        </a>
                        <a class="navbar-brand s-header__logo-link s-header__logo-img-shrink" href="{{route('parentServices')}}">
                           <h3 class="text-muted" style="margin-top:-15px;">{{$s->logo}}</h3>
                        </a>
                        @endforeach()
                     </div>
                  </div>
                  <div class="s-header__navbar-row-col">
                     <!-- Trigger -->
                     <a href="javascript:void(0);" class="s-header__trigger js__trigger">
                        <span class="s-header__trigger-icon"></span>
                        <svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
                           <circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
                        </svg>
                     </a>
                     <!-- End Trigger -->
                  </div>
               </div>
            </div>
         </div>
         <!-- End Navbar -->
         <!-- Overlay -->
         <div class="s-header-bg-overlay js__bg-overlay">
            <!-- Nav -->
            <nav class="s-header__nav js__scrollbar">
               <div class="container-fluid">
                  <!-- Menu List -->
                  </ul>
                  <!-- End Menu List -->
                  <!-- Menu List -->
                  <ul class="list-unstyled s-header__nav-menu">
                     <li class="s-header__nav-menu-item">
                        <h2 class="s-header__nav-menu-link "> Device Repair & Tech Support</h2>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="{{route('subServices',31)}}">Personal </a>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="{{route('subServices',1)}}">Home </a>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="#">FAQ</a>
                     </li>
                  </ul>
                  <!-- End Menu List -->
               </div>
            </nav>
            <!-- End Nav -->
            <!-- Action -->
            <ul class="list-inline s-header__action s-header__action--lb">
               <li class="s-header__action-item"><a class="s-header__action-link -is-active" href="#">En</a>
               </li>
               <li class="s-header__action-item"><a class="s-header__action-link" href="#">Fr</a>
               </li>
            </ul>
            <!-- End Action -->
            <!-- Action -->
            <ul class="list-inline s-header__action s-header__action--rb">
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-facebook"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Facebook</span>
                  </a>
               </li>
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-twitter"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Twitter</span>
                  </a>
               </li>
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-instagram"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Instagram</span>
                  </a>
               </li>
            </ul>
            <!-- End Action -->
         </div>
         <!-- End Overlay -->
      </header>
      <!--========== END HEADER ==========-->
      <!--========== SWIPER SLIDER ==========-->
      <!--========== SWIPER SLIDER ==========-->
     
      <div class="s-swiper js__swiper-one-item">
         <!-- Swiper Wrapper -->
          
         <div class="swiper-wrapper">
            @foreach($setting as $set)
            <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('{{asset($set->slider_image)}}')">
               <div class="container g-text-center--xs g-ver-center--xs">
                  <div class="g-margin-b-30--xs">
                     @if(session('msg'))
                     <h1 style="color: white">{{session('msg')}}</h1>
                     @endif()
                     <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Welcome to Tech Support</p>
                     <h1 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md g-color--white g-letter-spacing--1">{{$set->title}}</h1>
                     <p class="g-font-size-18--xs g-font-size-26--md g-color--white-opacity g-margin-b-0--xs">{{$set->title_description}}</p>
                     <br>
                     <span class="g-display-block--xs g-display-inline-block--sm g-padding-x-7--xs g-margin-b-10--xs g-margin-b-0--sm">
                     <a href="{{route('service')}}" class="text-uppercase s-btn s-btn-icon--md s-btn--white-bg g-radius--50 g-padding-x-65--xs"> Booking Now</a>
                     </span>
                     <span class="g-display-block--xs g-display-inline-block--sm g-padding-x-7--xs g-margin-b-10--xs g-margin-b-0--sm">
                     <a href="#js__scroll-to-section" class="text-uppercase s-btn s-btn-icon--md s-btn--white-bg g-radius--50 g-padding-x-65--xs">Get Started</a>
                     </span>
                  </div>
               </div>
               <!-- end of title-->
            </div>
               @endforeach()
         </div>
      
      </div>
      
      <!-- End Swiper Wrapper -->
      <!-- Arrows -->
      <a href="javascript:void(0);" class="s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-right js__swiper-btn--next"></a>
      <a href="javascript:void(0);" class="s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-left js__swiper-btn--prev"></a>
      <!-- End Arrows -->
      <!--========== END SWIPER SLIDER ==========-->
      <!--========== PROMO BLOCK ==========-->
      <!--========== END PROMO BLOCK ==========-->
      <!--========== PAGE CONTENT ==========-->
      <!-- Features -->
      <div class="g-bg-color--sky-light" id="fh5co-features" data-section="features">
         <div class="container">
            <div class="row">
               <div id="js__scroll-to-section2" class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
                  <h1 class="fh5co-lead to-animate">Our Services</h1>
                  <p class="fh5co-sub to-animate">We use the latest technologies to provide the best IT solutions that respond to the modern market needs. Whether you need IT support for your personal use or your business, you landed in the right place. Celfixx has your IT needs covered!</p>
               </div>
               @foreach($service as $s)
               <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
                  <a href="{{route('subServices',$s->id)}}" class="fh5co-feature to-animate">
                     <img src="{{$s->icon}}" class="img-responsive"    >
                     <h5 class="fh5co-feature-text">{{$s->name}}</h5>
                  </a>
               </div>
               @endforeach
               <div class="clearfix visible-sm-block"></div>
               <div class="clearfix visible-sm-block"></div>
               <div class="fh5co-spacer fh5co-spacer-sm"></div>
            </div>
         </div>
      </div>
      <!-- Features -->
      <div class="container g-padding-y-80--xs g-padding-y-125--sm">
         <div class="row g-margin-b-60--xs g-margin-b-70--md">
            <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
               <div class="clearfix">
                  <div class="g-media g-width-30--xs">
                     <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s"> <i class="g-font-size-28--xs g-color--primary ti-desktop"></i>
                     </div>
                  </div>
                  <div class="g-media__body g-padding-x-20--xs">
                     <h3 class="g-font-size-18--xs">Services</h3>
                     <p class="g-margin-b-0--xs">My Celfixx JLT is a leading mobile phone service center providing professional repairing services in Dubai, UAE. We have thousands of happy customers as of proof of the quality we provide to our customers.</p>
                  </div>
               </div>
            </div>
            <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
               <div class="clearfix">
                  <div class="g-media g-width-30--xs">
                     <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".2s"> <i class="g-font-size-28--xs g-color--primary ti-settings"></i>
                     </div>
                  </div>
                  <div class="g-media__body g-padding-x-20--xs">
                     <h3 class="g-font-size-18--xs">Repairs</h3>
                     <p class="g-margin-b-0--xs">We are adept in plenty of repair services like iPhone repair, iPad repair, iPod repair, iMac repair and MacBook repair. We also replace faulty battery, replace cracked and broken LCD, repair water damage or liquid damage phones etc.</p>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="clearfix">
                  <div class="g-media g-width-30--xs">
                     <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".3s"> <i class="g-font-size-28--xs g-color--primary ti-ruler-alt-2"></i>
                     </div>
                  </div>
                  <div class="g-media__body g-padding-x-20--xs">
                     <h3 class="g-font-size-18--xs">Innovation</h3>
                     <p class="g-margin-b-0--xs">Our technicians have always been keeping abreast of modern innovations multiplying everyday in smartphone and digital industries.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
               <div class="clearfix">
                  <div class="g-media g-width-30--xs">
                     <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".4s"> <i class="g-font-size-28--xs g-color--primary ti-package"></i>
                     </div>
                  </div>
                  <div class="g-media__body g-padding-x-20--xs">
                     <h3 class="g-font-size-18--xs">Pickup & Delivery</h3>
                     <p class="g-margin-b-0--xs">My Celfixx also understands that majority of our customers will prefer a pick-up and drop-off service because of their busy schedules. So, our pick-up and drop-off service are one of the primary reason why our customers love repairing with us.</p>
                  </div>
               </div>
            </div>
            <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
               <div class="clearfix">
                  <div class="g-media g-width-30--xs">
                     <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".5s"> <i class="g-font-size-28--xs g-color--primary ti-star"></i>
                     </div>
                  </div>
                  <div class="g-media__body g-padding-x-20--xs">
                     <h3 class="g-font-size-18--xs">User Satisfaction</h3>
                     <p class="g-margin-b-0--xs">My Celfixx is a service for the frustrated users who break their precious smartphone, iPhone, Macbook, iPad, iPod etc. The quality repair services by our technicians are carried out with extreme care.</p>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="clearfix">
                  <div class="g-media g-width-30--xs">
                     <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".6s"> <i class="g-font-size-28--xs g-color--primary ti-panel"></i>
                     </div>
                  </div>
                  <div class="g-media__body g-padding-x-20--xs">
                     <h3 class="g-font-size-18--xs">Customer Support</h3>
                     <p class="g-margin-b-0--xs">We provide same day repair and return in most cases. We have dedicated customer support team who believes in customer satisfaction more than anything else.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- End Features -->
      <!-- Process -->
      <div class="g-bg-color--sky-light g-bg-color--black">
         <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-text-center--xs g-margin-b-100--xs">
               <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--black-opacity g-letter-spacing--2 g-margin-b-25--xs">Process</p>
               <h2 class="g-font-size-32--xs g-font-size-36--md g-color--black">How it Works</h2>
            </div>
            <ul class="list-inline row g-margin-b-100--xs">
               <!-- Process -->
               <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                  <div class="center-block g-text-center--xs">
                     <div class="g-margin-b-30--xs"> <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--black g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">01</span>
                     </div>
                     <div class="g-padding-x-20--xs">
                        <h3 class="g-font-size-18--xs g-color--black">Consult</h3>
                        <p class="g-color--black-opacity">This is where we sit down, grab a cup of coffee and dial in the details.</p>
                     </div>
                  </div>
               </li>
               <!-- End Process -->
               <!-- Process -->
               <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                  <div class="center-block g-text-center--xs">
                     <div class="g-margin-b-30--xs"> <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--black g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">02</span>
                     </div>
                     <div class="g-padding-x-20--xs">
                        <h3 class="g-font-size-18--xs g-color--black">Create</h3>
                        <p class="g-color--black-opacity">The time has come to bring those ideas and plans to life.</p>
                     </div>
                  </div>
               </li>
               <!-- End Process -->
               <!-- Process -->
               <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--sm">
                  <div class="center-block g-text-center--xs">
                     <div class="g-margin-b-30--xs"> <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--black g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">03</span>
                     </div>
                     <div class="g-padding-x-20--xs">
                        <h3 class="g-font-size-18--xs g-color--black">Develop</h3>
                        <p class="g-color--black-opacity">Whether through commerce or just an experience to tell your brand.</p>
                     </div>
                  </div>
               </li>
               <!-- End Process -->
               <!-- Process -->
               <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1">
                  <div class="center-block g-text-center--xs">
                     <div class="g-margin-b-30--xs"> <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--black g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">04</span>
                     </div>
                     <div class="g-padding-x-20--xs">
                        <h3 class="g-font-size-18--xs g-color--black">Release</h3>
                        <p class="g-color--black-opacity">Now that your brand is all dressed up and ready to party.</p>
                     </div>
                  </div>
               </li>
               <!-- End Process -->
            </ul>
         </div>
      </div>
      <!-- End Process -->
      <!--code writing-->
      <!-- Testimonials -->
      <div class="g-hor-divider__dashed--sky-light g-padding-y-80--xs g-padding-y-125--xsm">
         <div class="container g-text-center--xs">
            <p class="text-uppercase g-font-size-20--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-50--xs">Why Choose Celfixx</p>
            <div class="s-swiper js__swiper-testimonials">
               <!-- Swiper Wrapper -->
               <div class="swiper-wrapper g-margin-b-50--xs">
                  <div class="swiper-slide g-padding-x-130--sm g-padding-x-150--lg">
                     <div class="g-padding-x-20--xs g-padding-x-50--lg">
                        <img class="g-width-100--xs g-height-80--xs g-hor-border-4__solid--white g-box-shadow__dark-lightest-v4 g-radius--circle g-margin-b-30--xs" src="img/400x400/google.png" alt="Image">
                        <div class="g-margin-b-40--xs">
                           <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--primary g-margin-b-5--xs">Google Rating</h4>
                           <p class="g-font-size-22--xs g-font-size-28--sm g-color--heading"><i>"Excellence in repair services and customer support enabled us to acquire thousands of satisfied customers as depicted in Google reviews."</i>
                           </p>
                        </div>
                        <div class="center-block g-hor-divider__solid--heading-light g-width-100--xs g-margin-b-30--xs"></div>
                     </div>
                  </div>
                  <div class="swiper-slide g-padding-x-130--sm g-padding-x-150--lg">
                     <div class="g-padding-x-20--xs g-padding-x-50--lg">
                        <img class="g-width-100--xs g-height-80--xs g-hor-border-4__solid--white g-box-shadow__dark-lightest-v4 g-radius--circle g-margin-b-30--xs" src="img/400x400/back.png" alt="Image">
                        <div class="g-margin-b-40--xs">
                           <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--primary g-margin-b-5--xs"> Warranty with Money Back Policy</h4>
                           <p class="g-font-size-22--xs g-font-size-28--sm g-color--heading"><i>" I have purchased many great templates over the years but this product and this company have taken it to the next level. Exceptional customizability. "</i>
                           </p>
                        </div>
                        <div class="center-block g-hor-divider__solid--heading-light g-width-100--xs g-margin-b-30--xs"></div>
                     </div>
                  </div>
                  <div class="swiper-slide g-padding-x-130--sm g-padding-x-150--lg">
                     <div class="g-padding-x-20--xs g-padding-x-50--lg">
                        <img class="g-width-100--xs g-height-80--xs g-hor-border-4__solid--white g-box-shadow__dark-lightest-v4 g-radius--circle g-margin-b-30--xs" src="img/400x400/expert.png" alt="Image">
                        <div class="g-margin-b-40--xs">
                           <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--primary g-margin-b-5--xs">Expert Technician</h4>
                           <p class="g-font-size-22--xs g-font-size-28--sm g-color--heading"><i>"Our certified technicians are electronics experts and have been trained and tested in all device repairs. They are continuously being trained in new repairs and repair processes that are up to the highest standards of My Celfixx JLT."</i>
                           </p>
                        </div>
                        <div class="center-block g-hor-divider__solid--heading-light g-width-100--xs g-margin-b-30--xs"></div>
                     </div>
                  </div>
               </div>
               <!-- End Swipper Wrapper -->
               <!-- Arrows -->
               <div class="g-font-size-22--xs g-color--heading js__swiper-fraction"></div>
               <a href="javascript:void(0);" class="g-display-none--xs g-display-inline-block--sm s-swiper__arrow-v1--right s-icon s-icon--md s-icon--primary-brd g-radius--circle ti-angle-right js__swiper-btn--next"></a>
               <a href="javascript:void(0);" class="g-display-none--xs g-display-inline-block--sm s-swiper__arrow-v1--left s-icon s-icon--md s-icon--primary-brd g-radius--circle ti-angle-left js__swiper-btn--prev"></a>
               <!-- End Arrows -->
            </div>
         </div>
      </div>
      <!-- End Testimonials -->
      <!-- Culture -->
      <!--code writing-->
      <!-- End Counter -->
      <!-- About -->
    <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="row g-hor-centered-row--md g-row-col--5 g-margin-b-80--xs g-margin-b-120--md">
                <div class="col-sm-6 col-xs-10 g-hor-centered-row__col">
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                        @foreach($setting as $set)
                      <img src="{{asset($set->picture_about)}}" alt="Image" class="img-responsive" align="middle" hspace="20" style="border: 14px solid white; ">
                        @endforeach()

                        
                    </div>
                </div>
                
                <div class="col-sm-1"></div>
                <div class="col-sm-5 g-hor-centered-row__col">
                    <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Culture</p>
                     <h2 class="g-font-size-32--xs g-font-size-36--sm g-margin-b-25--xs">About My Celfixx </h2>
                       @foreach($setting as $s)
                       <p class="g-font-size-18--sm" style="al">{{$s->about}}</p>
                       @endforeach()
                </div>
            </div>

      </div>
      <!-- End About -->
      <!-- Feedback Form -->
      <div class="g-bg-color--sky-light">
         <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div id="js__scroll-to-section" class="g-text-center--xs g-margin-b-80--xs">
               <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Contact Us</p>
               <h2 class="g-font-size-32--xs g-font-size-36--md">Let's Talk</h2>
            </div>
            <form action="{{route('contect')}}" method="post">
               {{csrf_field()}}
               <div class="row g-margin-b-40--xs">
                  <div class="col-sm-6 g-margin-b-20--xs g-margin-b-0--md">
                     <div class="g-margin-b-20--xs">
                        <input required name="name" type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Name">
                     </div>
                     <div class="g-margin-b-20--xs">
                        <input required name="email" type="email" class="form-control s-form-v2__input g-radius--50" placeholder="* Email">
                     </div>
                     <input required name="phone" type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Phone">
                  </div>
                  <div class="col-sm-6">
                     <textarea required name="message" class="form-control s-form-v2__input g-radius--10 g-padding-y-20--xs" rows="8" placeholder="* Your message"></textarea>
                     <span class="help-block">
                        <p id="characterLeft" class="help-block ">You have reached  the limit The limit is 120</p>
                     </span>
                  </div>
               </div>
               <div class="g-text-center--xs">
                  <button type="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs">Submit</button>
               </div>
            </form>
         </div>
      </div>
      <!-- End Feedback Form -->
      <!-- Google Map -->
      <!-- End Google Map -->
      <!--========== END PAGE CONTENT ==========-->
      <!--========== FOOTER ==========-->
      <footer class="g-bg-color--dark">
         <!-- Links -->
         <div class="g-hor-divider__dashed--white-opacity-lightest">
         <div class="container g-padding-y-80--xs">
            <div class="row">
               <div class="col-sm-4 g-margin-b-20--xs g-margin-b-0--md">
                  <h5 style="color: white;">Main Navigation</h5>
                  <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-0--xs">
                     <li><a class="g-font-size-15--xs g-color--white-opacity" href="#">Home</a>
                     </li>
                     <li><a class="g-font-size-15--xs g-color--white-opacity" href="#">About</a>
                     </li>
                     <li><a class="g-font-size-15--xs g-color--white-opacity" href="#">Work</a>
                     </li>
                     <li><a class="g-font-size-15--xs g-color--white-opacity" href="#">Contact</a>
                     </li>
                  </ul>
               </div>
               <div class="col-sm-4 g-margin-b-40--xs g-margin-b-0--md">
                  <h5 style="color: white;">Contact Info</h5>
                  <!-- <h6  style="color: white;">My Celfixx</h6> -->
                  <p class="glyphicon glyphicon-map-marker" style="color: white;">&nbsp;Location
                  <p style="color: white;">Gold Crest Executive Tower, Cluster No. C, Office No. 103, Jumeirah Lake Tower - Dubai, UAE</p>
                  </p>
                  <p class="glyphicon glyphicon-earphone" style="color: white;">&nbsp;PhoneNo
                  <p style="color: white;">+97144211494</p>
                  </p>
                  <p class="glyphicon glyphicon-envelope" style="color: white;">&nbsp;Email
                  <p style="color: white;">cs@mycelcare.com</p>
                  </p>
               </div>
               <div class="col-sm-4 g-margin-b-20--xs g-margin-b-0--md">
                  <h5 style="color: white;">Reviews</h5>
                  <h6 style="color: white;">
                     Cellfixx
                     <p> Power By Google</p>
                  </h6>
               </div>
            </div>
         </div>
         <!-- End Links -->
         <!-- Copyright -->
         <div class="container g-padding-y-50--xs">
            <div class="row">
               <div class="col-xs-6"></div>
               <div class="col-xs-6 g-text-right--xs">
                  <p class="g-font-size-14--xs g-margin-b-0--xs g-color--white-opacity-light">
                     <a href="#"></a>Powered by: <a href="#">Celfixx</a>
                  </p>
               </div>
            </div>
         </div>
         <!-- End Copyright -->
      </footer>
      <!--========== END FOOTER ==========-->
      <!-- Back To Top -->
      <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>
      <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
      <script type="text/javascript" async src="{{asset('js\all.js')}}"></script>
      <!-- General Components and Settings -->
      <!--========== END JAVASCRIPTS ==========-->
   </body>
   <!-- End Body -->
</html>