<!DOCTYPE html>
<html lang="en" class="no-js">
   <!-- Begin Head -->
   <head>
      <!-- Basic -->
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="keywords" content="Celfixx" />
      <meta name="description" content="celfixx provide the services for repairing mobile and tablet">
      <meta name="author" content="Celcare">
      <title>Celfixx</title>
      <!-- Web Fonts -->
      <link rel="icon" href="{{asset('img/fav.png')}}" type="image/gif" sizes="16x16">
      <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">
      <!-- Vendor Styles -->
      <!-- Theme Styles -->
      <link href="{{asset('css/style3.css')}}" rel="stylesheet" type="text/css" />
      <!-- Favicon -->
      <!-- <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon"> -->
   </head>
   <!-- End Head -->
   <!-- Body -->
   <body>
      <!--========== HEADER ==========-->
      <header class="navbar s-header js__header-sticky js__header-overlay">
         <!-- Navbar -->
         <div class="s-header__navbar">
            <div class="s-header__container">
               <div class="s-header__navbar-row">
                  <div class="s-header__navbar-row-col">
                     <div class="s-header__logo ">
                        <a class="navbar-brand s-header__logo-link s-header__logo-img-default" href="{{route('parentServices')}}">
                           <h3 class="text-muted">Celfixx</h3>
                        </a>
                        <a class="navbar-brand s-header__logo-link s-header__logo-img-shrink" href="{{route('parentServices')}}">
                           <h3 class="text-muted" style="margin-top:-15px;">Celfixx</h3>
                        </a>
                     </div>
                     <!-- Logo -->
                     <!-- <div class="s-header__logo">
                        <a href="{{route('parentServices')}}" class="s-header__logo-link">
                            <img class="s-header__logo-img s-header__logo-img-default" src="{{asset($setting->logo)}}" alt="Megakit Logo" >
                            <img class="s-header__logo-img s-header__logo-img-shrink" src="{{asset($setting->logo)}}" alt="Megakit Logo">
                        </a>
                        </div> -->
                     <!-- End Logo -->
                  </div>
                  <div class="s-header__navbar-row-col">
                     <!-- Trigger -->
                     <a href="javascript:void(0);" class="s-header__trigger js__trigger">
                        <span class="s-header__trigger-icon"></span>
                        <svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
                           <circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
                        </svg>
                     </a>
                     <!-- End Trigger -->
                  </div>
               </div>
            </div>
         </div>
         <!-- End Navbar -->
         <!-- Overlay -->
         <div class="s-header-bg-overlay js__bg-overlay">
            <!-- Nav -->
            <nav class="s-header__nav js__scrollbar">
               <div class="container-fluid">
                  <!-- Menu List -->
                  </ul>
                  <!-- End Menu List -->
                  <!-- Menu List -->
                  <ul class="list-unstyled s-header__nav-menu">
                     <li class="s-header__nav-menu-item">
                        <h2 class="s-header__nav-menu-link "> Device Repair & Tech Support</h2>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="{{route('subServices',31)}}">Personal </a>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="{{route('subServices',1)}}">Home </a>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="#">FAQ</a>
                     </li>
                  </ul>
                  <!-- End Menu List -->
               </div>
            </nav>
            <!-- End Nav -->
            <!-- Action -->
            <ul class="list-inline s-header__action s-header__action--lb">
               <li class="s-header__action-item"><a class="s-header__action-link -is-active" href="#">En</a>
               </li>
               <li class="s-header__action-item"><a class="s-header__action-link" href="#">Fr</a>
               </li>
            </ul>
            <!-- End Action -->
            <!-- Action -->
            <ul class="list-inline s-header__action s-header__action--rb">
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-facebook"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Facebook</span>
                  </a>
               </li>
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-twitter"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Twitter</span>
                  </a>
               </li>
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-instagram"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Instagram</span>
                  </a>
               </li>
            </ul>
            <!-- End Action -->
         </div>
         <!-- End Overlay -->
      </header>
      <!--========== END HEADER ==========-->
      <!-- Upcoming Event -->
      <style type="text/css">
         .hello {
         box-shadow: 0 5px 15px rgba(0,0,0,0), 0 5px 5px rgba(0,0,0,0.10);
         }
         .hello{
         height: 200px;
         }
      </style>
      <div class="container_flude g-bg-color--sky-light">
         <div class="row">
            <div class="col-sm-12 " style="height: 3000px;">
               <center>
                  <h2 class="g-color--black" style="margin-top: 10px;">What can we help with?</h2>
               </center>
               <?php $i=0; ?>@foreach($quick_service as $d)
               <div class="container  col-sm-2" id="zoom" style="padding: 18px;">
                  <div class="hello">
                     <center>
                        <a onclick="changePak(this,{{$i}})" href="#js__scrollbar1">
                           <img  src="{{asset($d->icon)}}" class="img-responsive " style="height: 150px; width: 150px;">
                     </center>
                     <center><h5 id="tech_{{$i}}">{{$d->name}}</h5></center></a>
                  </div>
               </div>
               <?php $i++; ?>@endforeach
            </div>
         </div>
      </div>
      <!--- sec 2 -->
      <div class="container_flude g-bg-color--sky-light " style="margin-top: 2000px;">
         <div class="row">
            <div class="col-sm-12 " style="height: 3000px;">
               <center>
                  <h2 class="g-color--black" id="js__scrollbar1" style="margin-top: 20px;">What the Tech happened?</h2>
               </center>
               <?php $i=0; ?>@foreach($issue as $d)
               <div class="container col-sm-2  " id="zoom" style="padding: 18px;">
                  <div class="hello">
                     <center>
                        <a onclick="changeValue(this,{{$i}})" href="#js__scrollbar2">
                           <img  src="{{asset($d->icon)}}" class="img-responsive " style="height: 150px; width: 150px;" >
                     </center>
                     <center><h5 id="technician_{{$i}}"  >{{$d->name}}</h5></center></a>
                  </div>
               </div>
               <?php $i++; ?>@endforeach
            </div>
         </div>
      </div>
      <!-- <form> -->
      <div class="container_flude g-bg-color--sky-light" style="margin-top: 2000px;">
         <div class="col-sm-12  well-lg g-bg-color--sky-light" id="js__scrollbar2">
            <center>
            <div class="form-area g-bg-color--sky-light">
               <form role="form" action="{{route('quick')}}" method="post">
                  {{csrf_field()}}
                  <br style="clear:both">
                  <h3 style="margin-bottom: 25px; text-align: center;">From phone and tablet repair to laptop support, we can do it all!</h3>
                  <div class="form-group">
                     <input type="text" id="ser" value="service" name="service" class="form-control" required>
                  </div>
                  <div class="form-group">
                     <input type="text" id="type" value="issue" name="issue" class="form-control" required>
                  </div>
                  <div class="form-group">
                     <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                  </div>
                  <div class="form-group">
                     <input required type="email" class="form-control" id="email" name="email" placeholder="Email">
                  </div>
                  <div class="form-group">
                     <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required>
                  </div>
                  <input name="location" placeholder="Search Your Location..." class="form-control col-sm-4" type="text" id="searchmap" name="searchmap" required>
                  <br>
                  <br>
                  <div style="width:100%;height:200px;" id="map"></div>
                  <br>
                  <button type="submit" id="submit" name="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs">Get a Quote</button>
                  <br>
                  <!-- form location detect -->
               </form>
            </div>
         </div>
      </div>
      <!-- svjsadfvsduf-->
      </div>
      </div>
      <!-- End Upcoming Event -->
      <!-- Back To Top -->
      <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>
      <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
      <!-- Vendor -->
      <script type="text/javascript" src="{{asset('js/all2.js')}}"></script>
      <!--========== END JAVASCRIPTS ==========-->
      <script>
         var map;function initMap(){map=new google.maps.Map(document.getElementById('map'),{center:{lat:-34.397,lng:150.644},zoom:8});var marker=new google.maps.Marker({position:{lat:-34.397,lng:150.644},map:map,draggable:!0});var searchBox=new google.maps.places.SearchBox(document.getElementById('searchmap'));google.maps.event.addListener(searchBox,'places_changed',function(){var places=searchBox.getPlaces();var bounds=new google.maps.LatLngBounds();var i,place;for(i=0;place=places[i];i++)
             {bounds.extend(place.geometry.location);marker.setPosition(place.geometry.location)}
             map.fitBounds(bounds);map.setZoom(15)});google.maps.event.addListener(marker,'position_changed',function(){var lat=marker.getPosition().lat();var lng=marker.getPosition().lng();$('#lat').val(lat);$('#lng').val(lng)})}
      </script>
      <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcoySGITCj_1UwWejhlrQqRmzOC8WeVHU&callback=initMap&libraries=places"></script>
      <script>
         function changeValue(elem,index){
             
             var x=document.getElementById('technician_' + index);
             console.log(x);
         // change the name of input
         document.getElementById('type').value=x.innerHTML;
         
         // change the other input
         
         }
      </script>
      <script>
         function changePak(elem,index){
             
             var x=document.getElementById('tech_' + index);
             
             // change the name of input
             document.getElementById('ser').value=x.innerHTML;
             
             // change the other input
             
         }
      </script>
   </body>
   <!-- End Body -->
</html>