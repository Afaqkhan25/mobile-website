<!DOCTYPE html>
<html lang="en" class="no-js">
   <!-- Begin Head -->
   <head>
      <!-- Basic -->
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="keywords" content="Celfixx" />
      <meta name="description" content="celfixx provide the services for repairing mobile and tablet">
      <meta name="author" content="Celcare">
      <title>Celfixx</title>
      <!-- Web Fonts -->
      <link rel="icon" href="{{asset('img/fav.png')}}" type="image/gif" sizes="16x16">
      <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">
      <!-- Vendor Styles -->
      <link href="{{asset('css/style3.css')}}" rel="stylesheet" type="text/css" />
      <!-- Theme Styles -->
      <!-- Favicon -->
      <!-- <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon"> -->
   </head>
   <!-- End Head -->
   <!-- Body -->
   <body>
      <!--========== HEADER ==========-->
      <header class="navbar-fixed-top s-header js__header-sticky js__header-overlay">
         <!-- Navbar -->
         <div class="s-header__navbar">
            <div class="s-header__container">
               <div class="s-header__navbar-row">
                  <div class="s-header__navbar-row-col">
                     <div class="s-header__logo ">
                        <a class="navbar-brand s-header__logo-link s-header__logo-img-default" href="{{route('parentServices')}}">
                           <h3 style="color: #f9f9f9;">Celfixx</h3>
                        </a>
                        <a class="navbar-brand s-header__logo-link s-header__logo-img-shrink" href="{{route('parentServices')}}">
                           <h3 class="text-muted" style="margin-top:-15px;">Celfixx</h3>
                        </a>
                     </div>
                  </div>
                  <div class="s-header__navbar-row-col">
                     <!-- Trigger -->
                     <a href="javascript:void(0);" class="s-header__trigger js__trigger">
                        <span class="s-header__trigger-icon"></span>
                        <svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
                           <circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
                        </svg>
                     </a>
                     <!-- End Trigger -->
                  </div>
               </div>
            </div>
         </div>
         <!-- End Navbar -->
         <!-- Overlay -->
         <div class="s-header-bg-overlay js__bg-overlay">
            <!-- Nav -->
            <nav class="s-header__nav js__scrollbar">
               <div class="container-fluid">
                  <!-- Menu List -->
                  </ul>
                  <!-- Menu List -->
                  <ul class="list-unstyled s-header__nav-menu">
                     <li class="s-header__nav-menu-item">
                        <h2 class="s-header__nav-menu-link "> Device Repair & Tech Support</h2>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="{{route('subServices',31)}}">Personal </a>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="{{route('subServices',1)}}">Home </a>
                     </li>
                     <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="#">FAQ</a>
                     </li>
                  </ul>
                  <!-- End Menu List -->
               </div>
            </nav>
            <!-- End Nav -->
            <!-- Action -->
            <ul class="list-inline s-header__action s-header__action--lb">
               <li class="s-header__action-item"><a class="s-header__action-link -is-active" href="#">En</a>
               </li>
               <li class="s-header__action-item"><a class="s-header__action-link" href="#">Fr</a>
               </li>
            </ul>
            <!-- End Action -->
            <!-- Action -->
            <ul class="list-inline s-header__action s-header__action--rb">
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-facebook"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Facebook</span>
                  </a>
               </li>
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-twitter"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Twitter</span>
                  </a>
               </li>
               <li class="s-header__action-item">
                  <a class="s-header__action-link" href="#"> <i class="g-padding-r-5--xs ti-instagram"></i>
                  <span class="g-display-none--xs g-display-inline-block--sm">Instagram</span>
                  </a>
               </li>
            </ul>
            <!-- End Action -->
         </div>
         <!-- End Overlay -->
      </header>
      <!--========== END HEADER ==========-->
      <!--========== PAGE CONTENT ==========-->
      <!--========== PROMO BLOCK ==========-->
      <!--   <img src="{{asset($details->picture)}}" class="img-responsive"> -->
      <div class="g-fullheight--xs js__parallax-window" style="background: url('{{ asset($details->picture) }}') 50% 0 no-repeat fixed; background-size: cover;">
         <div class="g-container--md g-ver-center--xs g-text-center--xs">
            <div class="g-margin-b-70--xs">
               <h1 class="g-font-size-100--sm g-font-size-90--lg g-font-family--playfair g-color--white g-margin-b-70--xs g-margin-b-100--sm">{{$details->description}}</h1>
            </div>
         </div>
      </div>
      <!--========== END PROMO BLOCK ==========-->
      <!-- Upcoming Event -->
      <div class="container_flude">
         <div class="row">
            <div class="col-md-6">
               <br>
               <h3 class="g-color--black" style="margin-left:10px; "> What can we help with?<br><br> 
                  Select Options
               </h3>
               <br>@forelse($data as $d)
               <div class="container col-sm-4 ">
                  <div id="zoom">
                     @if($d->depth==2)
                     <center>
                        <a href="#js__scrollbar1">
                           <button style="border:none; background: none;"   onclick="myFunction({{$d->id}})">
                              <img  src="{{asset($d->icon)}}" class="img-responsive "  >
                              <center>
                                 <h5 id="elem_{{ $d->id }}" >{{$d->name}}</h5>
                              </center>
                           </button>
                        </a>
                     </center>
                     @else
                     <center>
                        <a href="{{ route('subServices',$d->id)}}">
                           <img  src="{{asset($d->icon)}}" class="img-responsive " >
                           <center>
                              <h5 >{{$d->name}}</h5>
                           </center>
                        </a>
                     </center>
                     @endif()
                  </div>
               </div>
               @empty @endforelse
               <div class="g-margin-b-20--xs"></div>
            </div>
            <style type="text/css">
               .placeholder{color: grey;}
               select option:first-child{color: grey; display: none;}
               select option{color: #555;} // bootstrap default color
            </style>
            <!-- <form> -->
            <div class="container" id="js__scrollbar1">
               <div class="col-md-6 ">
                  <div class="form-area">
                     <form role="form" action="{{route('insert')}}" method="post">
                        {{csrf_field()}}
                        <br style="clear:both">
                        <h3 style="margin-bottom: 25px; text-align: center;">From phone and tablet repair to laptop support, we can do it all!</h3>
                        <div class="form-group">
                           @if($details!=null and $details->depth==0)
                           <select class="form-control placeholder" name="service">
                              <!-- <option  id="mySelect" class="name" value="">Select Service</option> -->
                              <option selected>{{$details->name}}</option>
                              @foreach(\DB::table('service')->where('id','<>',$details->id)->where('depth',0)->get() as $s)
                              <option data-sort="2">{{$s->name}}</option>
                              @endforeach()
                           </select>
                           @else
                           <select class="form-control placeholder" name="service">
                              @foreach(\DB::table('service')
                              ->where('id','<>',$details->id)
                              ->where('depth',0)->get() as $s)
                              <option data-sort="2">{{$s->name}}</option>
                              @endforeach
                           </select>
                           @endif()
                        </div>
                        <div class="form-group">
                           @if($details!=null and $details->depth==1)
                           <select class="form-control placeholder" name="model">
                              <!-- <option  id="mySelect" class="name" value="">Select Service</option> -->
                              <option selected>{{$details->name}}</option>
                              @foreach(\DB::table('service')->where('id','<>',$details->id)->where('depth',1)->get() as $s)
                              <option data-sort="2">{{$s->name}}</option>
                              @endforeach()
                           </select>
                           @else @endif()
                        </div>
                        <div class="form-group">
                           @if($details!=null and $details->depth==1)
                           <select class="form-control placeholder" name="issue">
                              <!-- <option  id="mySelect" class="name" value="">Select Service</option> -->
                              <option id="demo" selected>Select Issue</option>
                              @foreach(\DB::table('service')->where('id','<>',$details->id)->where('depth',2)->get() as $s)
                              <option data-sort="2">{{$s->name}}</option>
                              @endforeach()
                           </select>
                           @else @endif()
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                           <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required>
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" id="Comany" name="company" placeholder="Company" required>
                        </div>
                        <input name="location" placeholder="Search Your Location..." class="form-control col-sm-4" type="text" id="searchmap" name="searchmap">
                        <br>
                        <br>
                        <div style="width:100%;height:300px;" id="map"></div>
                        <br>
                        <div class="container form-group">
                           <input type="hidden" value="" name="company_id">
                           <label>
                           <p>Latitude</p>
                           <input class="form-control" placeholder="Latitude" type="text" name="lat" id="lat">
                           <p>Longitude</p>
                           <input class="form-control " type="text" id="lng" placeholder="Longitude" name="lng">
                        </div>
                        <br>
                        <div class="form-group">
                           <input type="text" class="form-control" id="code" name="promo code" placeholder="Promo Code">
                        </div>
                        <div class="form-group">
                           <textarea class="form-control" name="message" type="textarea" id="message" placeholder="Message" maxlength="140" rows="7" required></textarea>
                           <span class="help-block">
                              <p id="characterLeft" class="help-block ">You have reached the limit</p>
                           </span>
                        </div>
                        <button type="submit" id="submit" name="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs">Get a Quote</button>
                        <!-- form location detect -->
                     </form>
                  </div>
               </div>
            </div>
            <!-- svjsadfvsduf-->
         </div>
      </div>
      <!-- End Upcoming Event -->
      <br>
      <br>
      <br>
      <!-- Feedback Form -->
      <div class="g-bg-color--sky-light">
         <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-text-center--xs g-margin-b-80--xs">
               <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Contact Us</p>
               <h2 class="g-font-size-32--xs g-font-size-36--md">Let's Talk</h2>
            </div>
            <form action="{{route('contect')}}" method="post">
               {{csrf_field()}}
               <div class="row g-margin-b-40--xs">
                  <div class="col-sm-6 g-margin-b-20--xs g-margin-b-0--md">
                     <div class="g-margin-b-20--xs">
                        <input name="name" type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Name">
                     </div>
                     <div class="g-margin-b-20--xs">
                        <input name="email" type="email" class="form-control s-form-v2__input g-radius--50" placeholder="* Email">
                     </div>
                     <input name="phone" type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Phone">
                  </div>
                  <div class="col-sm-6">
                     <textarea name="message" class="form-control s-form-v2__input g-radius--10 g-padding-y-20--xs" rows="8" placeholder="* Your message"></textarea>
                     <span class="help-block">
                        <p id="characterLeft" class="help-block ">You have reached  the limit The limit is 120</p>
                     </span>
                  </div>
               </div>
               <div class="g-text-center--xs">
                  <button type="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs">Submit</button>
               </div>
            </form>
         </div>
      </div>
      <!-- End Feedback Form -->
      <!--========== END PAGE CONTENT ==========-->
      <!--========== FOOTER ==========-->
      <footer class="g-bg-color--dark">
         <!-- Links -->
         <div class="g-hor-divider__dashed--white-opacity-lightest">
         <div class="container g-padding-y-80--xs">
            <div class="row">
               <div class="col-sm-4 g-margin-b-20--xs g-margin-b-0--md">
                  <h5 style="color: white;">Main Navigation</h5>
                  <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-0--xs">
                     <li><a class="g-font-size-15--xs g-color--white-opacity" href="#">Home</a>
                     </li>
                     <li><a class="g-font-size-15--xs g-color--white-opacity" href="#">About</a>
                     </li>
                     <li><a class="g-font-size-15--xs g-color--white-opacity" href="#">Work</a>
                     </li>
                     <li><a class="g-font-size-15--xs g-color--white-opacity" href="#">Contact</a>
                     </li>
                  </ul>
               </div>
               <div class="col-sm-4 g-margin-b-40--xs g-margin-b-0--md">
                  <h5 style="color: white;">Contact Info</h5>
                  <!-- <h6  style="color: white;">My Celfixx</h6> -->
                  <p class="glyphicon glyphicon-map-marker" style="color: white;">&nbsp;Location
                  <p style="color: white;">Gold Crest Executive Tower, Cluster No. C, Office No. 103, Jumeirah Lake Tower - Dubai, UAE</p>
                  </p>
                  <p class="glyphicon glyphicon-earphone" style="color: white;">&nbsp;PhoneNo
                  <p style="color: white;">03326967721</p>
                  </p>
                  <p class="glyphicon glyphicon-envelope" style="color: white;">&nbsp;Email
                  <p style="color: white;">Afaqzai@gmail.com</p>
                  </p>
               </div>
               <div class="col-sm-4 g-margin-b-20--xs g-margin-b-0--md">
                  <h5 style="color: white;">Reviews</h5>
                  <h6 style="color: white;">
                     Cellfixx
                     <p> Power By Google</p>
                  </h6>
               </div>
            </div>
         </div>
         <!-- End Links -->
         <!-- Copyright -->
         <div class="container g-padding-y-50--xs">
            <div class="row">
               <div class="col-xs-6"></div>
               <div class="col-xs-6 g-text-right--xs">
                  <p class="g-font-size-14--xs g-margin-b-0--xs g-color--white-opacity-light">
                     <a href="#"></a>Powered by: <a href="#">Celfixx</a>
                  </p>
               </div>
            </div>
         </div>
         <!-- End Copyright -->
      </footer>
      <!--========== END FOOTER ==========-->
      <!-- Back To Top -->
      <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>
      <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
      <!-- Vendor -->
      <script type="text/javascript" async  src="{{asset('js/all1.js')}}"></script>
      <!--========== END JAVASCRIPTS ==========-->
      <script>
         var map;
         function initMap() {
           map = new google.maps.Map(document.getElementById('map'), {
             center: {lat: -34.397, lng: 150.644},
             zoom: 8
           });
           var marker=new google.maps.Marker({
            position:{
             lat: -34.397, 
             lng: 150.644
           },
           map:map,
           draggable: true
           
         });
           var searchBox= new google.maps.places.SearchBox(document.getElementById('searchmap'));
           google.maps.event.addListener(searchBox,'places_changed',function() {
            var places= searchBox.getPlaces();
            var bounds= new google.maps.LatLngBounds();
            var i,place;
            for(i=0;place=places[i];i++)
            {
             bounds.extend(place.geometry.location);
             marker.setPosition(place.geometry.location);
           }
           map.fitBounds(bounds);
           map.setZoom(15);
         //map.setCenter(marker.getPosition());
         });
           google.maps.event.addListener(marker,'position_changed',function(){
            var lat=marker.getPosition().lat();
            var lng= marker.getPosition().lng();
            
            
            $('#lat').val(lat);
            $('#lng').val(lng);
            
          });
         }
      </script>
      <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcoySGITCj_1UwWejhlrQqRmzOC8WeVHU&callback=initMap&libraries=places"></script>
      <script type="text/javascripts">
         onunload = function()
         {
           var foo = document.getElementById('foo');
           self.name = 'fooidx' + foo.selectedIndex;
         }
         
         onload = function()
         {
           var idx, foo = document.getElementById('foo');
           foo.selectedIndex = (idx = self.name.split('fooidx')) ? idx[1] : 0;
         }
      </script>
      <script type="text/javascript">
         $('select').change(function() {
          if ($(this).children('option:first-child').is(':selected')) {
            $(this).addClass('placeholder');
          } else {
           $(this).removeClass('placeholder');
         }
         });
      </script>
      <script type="text/javascript" >
         $(function(){
           $(document).on('click','input[type=text]',function(){ this.select(); });
         });
      </script>
      <script>
         $("#addName").click(function(){
           var myName = $("#nameInput").val();
           $(".name").html(myName);
         });
      </script>
      <script>
         function myFunction(elem) {
          var x=document.getElementById('elem_' + elem);
          
          var box=document.getElementById("demo").innerHTML = x.innerHTML;
          box.value=elem;
         }
      </script>
   </body>
   <!-- End Body -->
</html>