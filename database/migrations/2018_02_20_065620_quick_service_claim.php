<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuickServiceClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quick_claim', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service');
            $table->string('issue');
            $table->string('name');
            $table->string('email');
            $table->string('mobile');
            $table->string('location');
         



           
            $table->timestamps();
        });     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quick_claim'); 
    }
}
