<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Claims extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service');
            $table->string('model');
            $table->string('issue');
            $table->string('name');
            $table->string('email');
            $table->string('mobile');
            $table->string('company');
            $table->string('location');
            $table->string('promo_code');
            $table->string('message');
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim'); 
    }
}
