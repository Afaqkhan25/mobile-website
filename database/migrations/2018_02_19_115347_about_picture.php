<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AboutPicture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         
        Schema::table('setting', function (Blueprint $table) {
        $table->string('title');
        $table->string('title_description');
        $table->string('picture_about');


     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setting', function($table) {
         $table->dropColumn('title');
         $table->dropColumn('title_description');
        $table->dropColumn('picture_about');

});
    }
}
