let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
   

*/

   /*index.php file css*/

mix.styles([

    'public/vendor/bootstrap/css/bootstrap.min.css',
    'public/css/animate.css',
    'public/vendor/themify/themify.css',
    'public/vendor/scrollbar/scrollbar.min.css',
    'public/vendor/magnific-popup/magnific-popup.css',
    'public/vendor/swiper/swiper.min.css',
    'public/vendor/cubeportfolio/css/cubeportfolio.min.css',
    'public/css/style.css',
    'public/css/global/global.css'

], 'public/css/style.css');


    /*personal.php file css*/

mix.styles([

    'public/vendor/bootstrap/css/bootstrap.min.css',
    'public/css/animate.css',
    'public/vendor/themify/themify.css',
    'public/vendor/scrollbar/scrollbar.min.css',
    'public/vendor/swiper/swiper.min.css',
    'public/vendor/cubeportfolio/css/cubeportfolio.min.css',
    'public/css/style.css',
    'public/css/global/global.css'

], 'public/css/style2.css');
 
 /*service.php file css*/

mix.styles([

    'public/vendor/bootstrap/css/bootstrap.min.css',
    'public/css/animate.css',
    'public/vendor/themify/themify.css',
    'public/vendor/scrollbar/scrollbar.min.css',
    'public/vendor/swiper/swiper.min.css',
    'public/vendor/cubeportfolio/css/cubeportfolio.min.css',
    'public/css/style.css',
    'public/css/global/global.css'

], 'public/css/style3.css');





    /*index.php file javascript*/

mix.scripts([
    'public/vendor/jquery.min.js',
    'public/vendor/jquery.migrate.min.js',
    'public/vendor/bootstrap/js/bootstrap.min.js',
    'public/vendor/jquery.smooth-scroll.min.js',
    'public/vendor/jquery.back-to-top.min.js',
    'public/vendor/scrollbar/jquery.scrollbar.min.js',
    'public/vendor/magnific-popup/jquery.magnific-popup.min.js',
    'public/vendor/swiper/swiper.jquery.min.js',
    'public/vendor/waypoint.min.js',
    'public/vendor/counterup.min.js',
    'public/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js',
    'public/vendor/jquery.parallax.min.js',
    'public/vendor/jquery.wow.min.js',
    'public/js/global.min.js',
    'public/js/components/header-sticky.min.js',
    'public/js/components/scrollbar.min.js',
    'public/js/components/magnific-popup.min.js',
    'public/js/components/swiper.min.js',
    'public/js/components/counter.min.js',
    'public/js/components/portfolio-3-col.min.js',
    'public/js/components/parallax.min.js',
    'public/js/components/wow.min.js'

], 'public/js/all.js');


    /*personal.php file javascript*/

mix.scripts([
    'public/vendor/jquery.min.js',
    'public/vendor/jquery.migrate.min.js',
    'public/vendor/bootstrap/js/bootstrap.min.js',
    'public/vendor/jquery.smooth-scroll.min.js',
    'public/vendor/jquery.back-to-top.min.js',
    'public/vendor/scrollbar/jquery.scrollbar.min.js',
    'public/vendor/vidbg.min.js',
    'public/vendor/swiper/swiper.jquery.min.js',
    'public/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js',
    'public/vendor/jquery.wow.min.js',
    'public/js/global.min.js',
    'public/js/components/header-sticky.min.js',
    'public/js/components/scrollbar.min.js',
    'public/js/components/swiper.min.js',
    'public/js/components/portfolio-4-col-slider.min.js',
    'public/js/components/wow.min.js'

], 'public/js/all1.js');


 /*service.php file javascript*/

mix.scripts([
    'public/vendor/jquery.min.js',
    'public/vendor/jquery.migrate.min.js',
    'public/vendor/bootstrap/js/bootstrap.min.js',
    'public/vendor/jquery.smooth-scroll.min.js',
    'public/vendor/jquery.back-to-top.min.js',
    'public/vendor/scrollbar/jquery.scrollbar.min.js',
    'public/vendor/vidbg.min.js',
    'public/vendor/swiper/swiper.jquery.min.js',
    'public/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js',
    'public/vendor/jquery.wow.min.js',
    'public/js/global.min.js',
    'public/js/components/header-sticky.min.js',
    'public/js/components/scrollbar.min.js',
    'public/js/components/swiper.min.js',
    'public/js/components/portfolio-4-col-slider.min.js',
    'public/js/components/wow.min.js'

], 'public/js/all2.js');












