<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    protected $table='claim';
    protected $primarykey='id';
    protected $fillables=['service','model','issue','name','email','mobile','location','promo_code','message','company'];
}
