<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\quick_claim;

class QuickClaimController extends Controller
{
    public function insert(Request $r)
	{
          $claim= new quick_claim();
          $claim->service= $r->service;
          $claim->issue=$r->issue;
          $claim->name=$r->name;
          $claim->email=$r->email;
          $claim->mobile=$r->mobile;
          $claim->location=$r->location;
        	$claim->save();

	return redirect()->route('parentServices')->with('msg','Successfully submitted your request');
	}
    public function show()

    {
      $quick_claim=quick_claim::all();
    return view('admin/quickAdmin' , compact('quick_claim'));
    }


}
