<?php

namespace App\Http\Middleware;

use Closure;

class CheckDepth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $service=\DB::table('service')->where('id',$request->id)->get()->first();
       
        if($service->depth==2){
                return redirect()->back();
        }else{
            return $next($request);
        }
        
    }
}
