<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quick_claim extends Model
{
      protected $table='quick_claim';
    protected $primarykey='id';
    protected $fillables=['service','issue','name','email','mobile','location'];
}
