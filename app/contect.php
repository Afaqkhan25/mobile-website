<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contect extends Model
{
    protected $table='contect';
    protected $primarykey='id';
    protected $fillables=['name','email','phone','message'];
}
