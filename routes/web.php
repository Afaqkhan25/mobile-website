<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
	$service=DB::table('service')->where('depth',0)->where('parent_id',null)->orWhere('parent_id',0)->get();
	$setting=DB::table('setting')->get();

	
	
	// dd($service);
          return view('include/index',compact('service','setting'));
})->name('parentServices');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/personal-service/{id}', function ($id) {

	$details=DB::table('service')->where('id',$id)->get()->first();
	// $pic=DB::table('service')->where('id',$id)->get();
	$setting=DB::table('setting')->get()->first();
          
	$data=DB::table('service')->where('parent_id',$id)->get();
	
	
	return view('include/personal-service',compact('data','details','$pic','setting','id'));


	
})->name('subServices');
// ->middleware('check-depth');

Route::post('/insert','ClaimController@insert')->name('insert');

// Route::post('/contect','ContectController@contect')->name('contect');
Route::post('/contect','ContectController@contect')->name('contect');


Route::get('/service', function(){
          $service=DB::table('service')->where('depth',0)->where('parent_id',null)->orWhere('parent_id',0)->get();
	$setting=DB::table('setting')->get()->first();
	$details=DB::table('service')->get()->first();
	// $pic=DB::table('service')->where('id',$id)->get();
	$setting=DB::table('setting')->get()->first();
          $quick_service=DB::table('quick_service')->get();
	$data=DB::table('service')->get();
	$issue=DB::table('quick_service_issue')->get();
	
    return view('include/service',compact('service','setting','details','data','quick_service','issue'));
})->name('service');

Route::post('/insert_quick','QuickClaimController@insert')->name('quick');

/////Admin Pannel

Route::get('/admin',function(){
return view('/admin/adminhome');
})->middleware('auth');

Route::get('/claim_admin','ClaimController@show')->name('claim_admin')->middleware('auth');

Route::get('/contect_admin','ContectController@show')->name('contect_admin')->middleware('auth');

Route::get('/quick_claim','QuickClaimController@show')->name('quick_claim')->middleware('auth');

Route::get('/services_admin',function(){
$service=DB::table('service')->get();
return view('/admin/serviceAdmin',compact('service'));
})->name('services_admin')->middleware('auth');


Route::get('/setting_admin',function(){
$setting=DB::table('setting')->get();
return view('/admin/settingAdmin',compact('setting'));

})->name('setting_admin')->middleware('auth');

Route::get('/quick_service',function(){
$quick_service=DB::table('quick_service')->get();
return view('/admin/quick_serviceAdmin',compact('quick_service'));

})->name('quick_Service')->middleware('auth');


Route::get('/quick_issue',function(){
$quick_issue=DB::table('quick_service_issue')->get();
return view('/admin/quick_issueAdmin',compact('quick_issue'));

})->name('quick_issue')->middleware('auth');



Route::post('/save_settings',function(Request $r){
	

	DB::table('setting')->insert(
    ['logo' => $r->logo, 'about' => $r->about, 'title'=>$r->title, 'title_description'=>$r->title_description,'picture_about'=>$r->picture_about,'slider_image'=>$r->slider_image]
);

         
})->middleware('auth');




Route::post('/save_quick_service',function(Request $r){
 
 DB::table('quick_service')->insert(
    ['icon' => $r->icon, 'name' => $r->name]
);

})->middleware('auth');


Route::post('/save_quick_issue',function(Request $r){
 
 DB::table('quick_service_issue')->insert(
    ['icon' => $r->icon, 'name' => $r->name]
);

})->middleware('auth');



Route::post('/update_settings',function(Request $r){

	DB::table('setting')
            ->where('id',$r->id)
            ->update(['logo' => $r->logo, 'about' => $r->about, 'title'=>$r->title, 'title_description'=>$r->title_description,'picture_about'=>$r->picture_about,'slider_image'=>$r->slider_image]);

})->middleware('auth');



Route::get('/setting/delete/{id}',function($id){

	DB::table('setting')->where('id', '=',$id)->delete();
	return redirect()->route('setting_admin');
	})->middleware('auth')->name('setting_delete');


Route::post('/update_quick_service',function(Request $r){

            DB::table('quick_service')
            ->where('id',$r->id)
            ->update(['icon' => $r->icon, 'name' => $r->name]);

return response()->json($r->logo);
})->middleware('auth');

Route::get('/quick_service/delete/{id}',function($id){

	DB::table('quick_service')->where('id', '=',$id)->delete();
	return redirect()->route('quick_Service');
	})->middleware('auth')->name('quick_service_delete');


Route::post('/update_quick_issue',function(Request $r){

            DB::table('quick_service_issue')
            ->where('id',$r->id)
            ->update(['icon' => $r->icon, 'name' => $r->name]);

return response()->json($r->logo);
})->middleware('auth');


Route::get('/quick_service_issue/delete/{id}',function($id){

	DB::table('quick_service_issue')->where('id', '=',$id)->delete();
	return redirect()->route('quick_issue');
	})->middleware('auth')->name('quick_service_issue_delete');

Route::post('/update_contact',function(Request $r){

            DB::table('contect')
            ->where('id',$r->id)
            ->update(['name' => $r->name, 'email' => $r->email,'phone'=>$r->phone,'message'=>$r->message]);

return response()->json($r->logo);
})->middleware('auth');

Route::get('/contact/delete/{id}',function($id){

	DB::table('contect')->where('id', '=',$id)->delete();
	return redirect()->route('contect_admin');
	})->middleware('auth')->name('contect_delete');

Route::post('/update_quick_claim',function(Request $r){

            DB::table('quick_claim')
            ->where('id',$r->id)
            ->update(['service' => $r->service, 'issue' => $r->issue,'name'=>$r->name,'email'=>$r->email,'mobile'=>$r->mobile,'location'=>$r->location]);

return response()->json($r->logo);
})->middleware('auth');

Route::get('/quick_claim/delete/{id}',function($id){

	DB::table('quick_claim')->where('id', '=',$id)->delete();
	return redirect()->route('quick_claim');
	})->middleware('auth')->name('quick_claim_delete');

Route::post('/update_claim',function(Request $r){

            DB::table('claim')
            ->where('id',$r->id)
            ->update(['service' => $r->service, 'model' => $r->model,'issue'=>$r->issue,'name'=>$r->name,'email'=>$r->email,'mobile'=>$r->mobile,'company'=>$r->company,'location'=>$r->location,'promo_code'=>$r->promo_code,'message'=>$r->message]);

return response()->json($r->logo);
})->middleware('auth');


Route::get('/claim/delete/{id}',function($id){

	DB::table('claim')->where('id', '=',$id)->delete();
	return redirect()->route('claim_admin');
	})->middleware('auth')->name('claim_delete');

//////////////// service save update and delete

Route::post('/save_service',function(Request $r){
	
	$depth=getDepth($r->parent_id,0);

	DB::table('service')->insert(
    	[
    		'icon' => $r->icon, 
    		'name' => $r->name, 
    		'parent_id'=>$r->parent_id,
    		'picture'=>$r->picture,
    		'description'=>$r->description,
    		'depth'=>$depth
    	]);

	return response()->json($r->logo);
})->middleware('auth');

Route::post('/update_service',function(Request $r){

            DB::table('service')
            ->where('id',$r->id)
            ->update(['icon' => $r->icon, 'name' => $r->name, 'parent_id'=>$r->parent_id,'picture'=>$r->picture,'description'=>$r->description]);


})->middleware('auth');

Route::get('/service/delete/{id}',function($id){

	DB::table('service')->where('id', '=',$id)->delete();
	return redirect()->route('services_admin');
})->middleware('auth')->name('service_delete');

function getDepth($id,$i=0){
	
	$cat=DB::table('service')->where('id',$id)->get()->first();
	
	if($cat){
		$i++;
		/*$subCat=DB::table('service')->where('id',$cat->parent_id)->first();
	

		if($subCat){
			$i++;*/
			
			return getDepth($cat->parent_id,$i);
		
		/*}else{
			return $i;
		}*/
	}else{
		return $i;
	}
	/*else{
		$subCat=$subCat=DB::table('service')->where('id',$id)->first();

		if($subCat){
			$i++;
			
			return getDepth($subCat->id,$i);
		
		}else{
			return $i;
		}
	}*/
	
}
